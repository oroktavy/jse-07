package ru.aushakov.tm.model;

import ru.aushakov.tm.constant.ArgumentConst;
import ru.aushakov.tm.constant.TerminalConst;

public class Command {

    public static final Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT,
            ArgumentConst.ARG_ABOUT,
            "Show general application info"
    );

    public static final Command VERSION = new Command(
            TerminalConst.CMD_VERSION,
            ArgumentConst.ARG_VERSION,
            "Show application version"
    );

    public static final Command HELP = new Command(
            TerminalConst.CMD_HELP,
            ArgumentConst.ARG_HELP,
            "Show possible options"
    );

    public static final Command INFO = new Command(
            TerminalConst.CMD_INFO,
            ArgumentConst.ARG_INFO,
            "Show system information"
    );

    public static final Command EXIT = new Command(
            TerminalConst.CMD_EXIT,
            null,
            "Close application"
    );

    private String name = "";

    private String arg = "";

    private String description = "";

    public Command() {
    }

    public Command(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        if (this.name != null && !this.name.isEmpty()) result += this.name;
        if (this.arg != null && !this.arg.isEmpty()) result += ": [" + this.arg + "]";
        if (this.description != null && !this.description.isEmpty()) result += " - " + this.description;
        return result;
    }
}
